# USAGE

**python3 convert.py /path/to/file.midi** *[--doubleStops:higher/multiple]*

* arguments
	* '--doubleStops'
		*	--doubleStops:higher - use higher note if double stops are found (default)
		*	--doubleStops:multiple - use all notes in double stops and loose higher overtones

target file will be next to source file with file-extension .txt