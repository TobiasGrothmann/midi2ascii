import copy

class Partial:
	def __init__(self, freq, amp, instrument):
		self.freq = freq
		self.amp = amp
		self.instrument = instrument

	def __str__(self):
		return "%i %i %i" % (self.freq, max(min(int(round(self.amp * 1000)), 1000), 0), self.instrument) # for float use: %.2f
	def __repr__(self):
		return str(self)
	def __lt__(self, other):
		return self.freq < other.freq


sampleAmps = [1.0, 0.5366543450515755, 0.2339087671852954, 0.3016963805656517, 0.20433765261673575, 0.2345546785316873, 0.03851127968831187, 0.10535926930145895]
sampleFreqs = [497.955322, 995.910645, 1487.809753, 1985.765076, 2495.832825, 2997.15271, 3473.574829, 3990.371704]
sampleNoteMidi = 71

class PartialList:
	def __init__(self, midi, instrument):
		self.partials = list(map(lambda i: Partial(sampleFreqs[i] * (2**(1/12))**(midi-sampleNoteMidi), sampleAmps[i], instrument), range(8)))
		self.instrument = instrument
	
	@staticmethod
	def fromMultiple(midis, instrument):
		if (len(midis) != 2):
			print("ERROR: only two notes at once supported atm")
			return None
		newNote = PartialList(midis[0], instrument)
		newNoteOther = PartialList(midis[1], instrument)
		newNote.partials = newNote.partials[:4] + newNoteOther.partials[:4]
		if (len(newNote.partials)) != 8:
			print("ERROR: something went wrong, need 8 partials but got: %i" % len(newNote.partials))
			return None
		return newNote
	
	@staticmethod
	def createEmpty(instrument):
		newNote = PartialList(1, instrument)
		newNote.partials = [Partial(0, 0, instrument)] * 8
		return newNote

	def __str__(self):
		return " ".join(list(map(lambda p: str(p), self.partials)))
	def __repr__(self):
		return str(self)

class Event:
	def __init__(self, notes, label, timeDif):
		if (len(notes) != 4): print("WARNING: must be 4 notes")
		self.notes = notes
		self.label = label
		self.timeDif = timeDif

	def getFreqDistTo(self, other):
		dif = 0
		if (len(self.notes) != 4):
			print("WARNING: len(self.notes) not 4 but %i" % len(self.notes))
		for n in range(4):
			if (len(self.notes[n].partials) != 8):
				print("WARNING: len(self.notes[%i]) not 8 but %i" % (n, len(self.notes[n])))
			for o in range(8):
				# dif += abs((self.notes[n].partials[o].freq / other.notes[n].partials[o].freq) - 1)
				dif += abs(self.notes[n].partials[o].freq - other.notes[n].partials[o].freq)
		return dif

	def getPartialList(self):
		allPartials = []
		for note in self.notes:
			allPartials.extend(copy.deepcopy(note.partials))
		return allPartials

	def setPartialsByList(self, partials):
		partials = copy.deepcopy(partials)
		for a in range(4):
			for b in range(8):
				self.notes[a].partials[b] = partials[a * 8 + b]

	def minimizeFreqDistTo(self, other):
		target = other.getPartialList()
		permutationToSortTarget = [i[0] for i in sorted(enumerate(target), key=lambda x: x[1])]
		permutationNeeded = [i[0] for i in sorted(enumerate(permutationToSortTarget), key=lambda x: x[1])]

		start = self.getPartialList()
		start.sort()
		new = [start[i] for i in permutationNeeded]
		self.setPartialsByList(new)

	def __str__(self):
		return "[target]\n%s\n%.6f\n%s" % (self.label, self.timeDif, "\n".join(map(lambda n: str(n), self.notes)))
