#!/usr/bin/python
from __future__ import absolute_import
import sys, os
from mido import MidiFile, open_output, second2tick
from MIDI2ASCII import *
import copy


########## UTIL
def printUsage():
	print("\nUSAGE:\n\targuments: midiFile [--doubleStops:higher/multiple]")
	print("\t\tmidiFile - absolute or relative path to .mid file")
	print("\t\t--doubleStops")
	print("\t\t\t--doubleStops:higher - use higher note if double stops are found (default)")
	print("\t\t\t--doubleStops:multiple - use all notes in double stops and loose higher overtones")
	print("")
def fatal(message):
	print("\nERROR: %s" % message)
	printUsage()
	exit(1)
def log(message):
	print("\t%s" % message)
def logSection(message):
	print("%s" % message.upper())






########## SETTINGS
if (len(sys.argv) < 1 + 1):
	fatal("need at least 1 argument: source - but got %i" % (len(sys.argv) - 1))

doubleStopHandling = "higher"

sources = []
targets = []
for arg in sys.argv[1:]:
	if os.path.exists(arg):
		if not os.path.isfile(arg):
			fatal("'%s' is not a file" % arg)
		sourceExtension = os.path.splitext(os.path.basename(arg))[1]
		if (os.path.splitext(arg)[1] not in [".mid", ".midi"]):
			fatal("source path (%s) must be midi file but is: '%s'" % (arg, sourceExtension))

		targets.append(os.path.join(os.path.dirname(arg), os.path.splitext(os.path.basename(arg))[0] + ".txt"))
		sources.append(arg)
		continue

	flagSplit = arg.split(":")
	if (len(flagSplit) != 2):
		fatal("cannot parse argument: '%s'" % arg)

	if (flagSplit[0] == "--doubleStops"):
		if (flagSplit[1] not in ["higher", "multiple"]):
			fatal("invalid option for --doubleStops: '%s'" % flagSplit[1])
		doubleStopHandling = flagSplit[1]
	else:
		fatal("invalid argument: '%s'" % arg)

if (doubleStopHandling == "multiple"):
	fatal("sorry doubleStopHandling multiple is not supported yet")

logSection("settings")
for i in range(len(sources)):
	log("source: %s -> %s" % (sources[i], targets[i]))	
log("double stop handling: %s" % doubleStopHandling)








########## CONVERSION
for i in range(len(sources)):
	source = sources[i]
	target = targets[i]
	logSection("converting %i/%i" % (i+1, len(sources)))
	midiFile = MidiFile(source)
	log("ticks per beat: %i" % midiFile.ticks_per_beat)

	allEvents = {}
	totalTime = 0.0
	tempo = 1
	for msg in midiFile:
		if msg.type == "set_tempo":
			tempo = msg.tempo
		deltaTicks = second2tick(msg.time, midiFile.ticks_per_beat, tempo)
		deltaTime = (deltaTicks / midiFile.ticks_per_beat) * 4.0
		totalTime += deltaTime
		if (msg.type == "note_on" and msg.velocity != 0): # noteOn with vel:0 should be treated as noteOff
			if msg.channel not in [1, 2, 3, 4]:
				fatal("only channel 1, 2, 3 and 4 are supported (but got %i)" % msg.channel)
			if (totalTime not in allEvents):
				allEvents[totalTime] = []
			allEvents[totalTime].append(msg)

	if doubleStopHandling == "higher":
		log("dropping lower notes in double stops")
		droppedFromChannel = {}
		for timestamp in allEvents:
			if len(allEvents[timestamp]) > 4:
				cleanedNotes = []
				for note in allEvents[timestamp]:
					notesOfSameChannel = list(filter(lambda n: n.channel == note.channel, cleanedNotes))
					if len(notesOfSameChannel) != 0:
						if (notesOfSameChannel[0].note < note.note):
							cleanedNotes[cleanedNotes.index(notesOfSameChannel[0])] = note
						if (note.channel) not in droppedFromChannel:
							droppedFromChannel[note.channel] = 0
						droppedFromChannel[note.channel] += 1
					else:
						cleanedNotes.append(note)
				allEvents[timestamp] = cleanedNotes
		droppedFromChannel = {k: droppedFromChannel[k] for k in sorted(droppedFromChannel)} # sort by channel
		for key in droppedFromChannel:
			log("\tdropped %i notes from channel %i" % (droppedFromChannel[key], key))


	lastPartials = [PartialList.createEmpty(1), PartialList.createEmpty(2), PartialList.createEmpty(3), PartialList.createEmpty(4)]
	outEvents = [Event(lastPartials, "start", 0)]
	lastPartialsNotOptimized = copy.deepcopy(lastPartials)

	i = 0
	lastTime = 0
	for time in allEvents:
		i += 1
		partialLists = list(map(lambda n: PartialList(n.note, n.channel), allEvents[time]))

		for partialList in partialLists:
			lastPartialsNotOptimized[partialList.instrument - 1] = partialList

		newEvent = Event(copy.deepcopy(lastPartialsNotOptimized), "", time - lastTime)

		newEvent.minimizeFreqDistTo(copy.deepcopy(outEvents[-1]))

		newEvent.label = "i: %i - freqDistSum: %.0f" % (i, newEvent.getFreqDistTo(outEvents[-1]))
		outEvents.append(newEvent)
		lastTime = time

	########## OUTPUT
	with open(target, "w") as file:
		log("writing %i events to: %s" % (len(outEvents), target))
		for event in outEvents:
			file.write("%s\n\n" % str(event))


logSection("done")
