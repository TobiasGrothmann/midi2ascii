(
var sampleAmps = [1.0, 0.5366543450515755, 0.2339087671852954, 0.3016963805656517, 0.20433765261673575, 0.2345546785316873, 0.03851127968831187, 0.10535926930145895];
var sampleFreqs = [497.955322, 995.910645, 1487.809753, 1985.765076, 2495.832825, 2997.15271, 3473.574829, 3990.371704];

sampleAmps.do{ |amp, i|
	var freq = sampleFreqs[i];
	[freq, amp].postln;
	{
		var freqRange = 0.007;
		// freq = SinOsc.kr(LFNoise2.kr(1).range(6, 7)).range(freq * (1.0-freqRange), freq * (1.0+freqRange));
		freq = SinOsc.kr(7).range(freq * (1.0-freqRange), freq * (1.0+freqRange));
		freq = LFNoise2.kr(9).range(freq * (1.0-freqRange), freq * (1.0+freqRange));
		Pan2.ar(SinOscFB.ar(freq, LFNoise2.kr(5).range(0.55, 0.7)) * amp * (1.0/8), ((i+0.5)/8.0))
	}.play
}
)

s.meter
s.reboot

(freq: 71.midicps).play

s.makeWindow